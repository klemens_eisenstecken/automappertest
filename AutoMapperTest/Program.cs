﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using System.Diagnostics;

namespace AutoMapperTest
{
    class Program
    {
        static void Main(string[] args)
        {
            IConfigurationProvider config = new MapperConfiguration(c => {
                c.CreateMap<Test, TestDTO>(MemberList.Destination)
                    .ForMember(d => d.MyIntProp1, a => a.MapFrom(s => s.MyIntProp1))
                    .ForMember(d => d.MySuperFancyIntProp2, a => a.MapFrom(s => s.MyIntProp2))
                    .ForMember(d => d.MyEnumerable, a => a.MapFrom(s => s.MyEnumerable));
                c.CreateMap<TestEnumerable, TestEnumerableDTO>()
                    .ForMember(d => d.MyProperty, a => a.MapFrom(s => s.MyProperty));
                c.ConstructServicesUsing(testIntf =>
                {
                    return null;
                });
                //c.CreateMap<ITestInterface, ITestInterfaceDTO>().As<MyTestInterfaceDTO>();
                //c.CreateMap<ITestInterface, ITestInterfaceDTO>().As<MyTestInterfaceDTO2>();
                //c.CreateMap<ITestInterface, ITestInterfaceDTO>()
                //    .ConstructUsing((type, context) =>
                //    {
                //        if (type is MyTestInterface)
                //        {
                //            return context.Mapper.Map<MyTestInterfaceDTO>(type);
                //        }
                //        else if (type is MyTestInterface2)
                //        {
                //            return context.Mapper.Map<MyTestInterfaceDTO2>(type);
                //        }

                //        return null;
                //    });
                c.CreateMap<ITestInterface, ITestInterfaceDTO>();
                //    .Include<MyTestInterface, MyTestInterfaceDTO>()
                //    .Include<MyTestInterface2, MyTestInterfaceDTO2>();
                c.CreateMap<MyTestInterface, MyTestInterfaceDTO>()
                    .IncludeBase<ITestInterface, ITestInterfaceDTO>();
                c.CreateMap<MyTestInterface2, MyTestInterfaceDTO2>()
                    .IncludeBase<ITestInterface, ITestInterfaceDTO>();
                c.CreateMap<SubType, SubTypeDTO>();
                c.CreateMap<CtorSubType, CtorSubTypeDTO>();
            });

            config.AssertConfigurationIsValid();
            config.CompileMappings();

            IMapper mapper = config.CreateMapper();

            var val = new Test()
            {
                MyIntProp1 = 1,
                MyIntProp2 = 2,
                MyCtorSubType = new CtorSubType(5000),
                MySubTypeProp = new SubType()
                {
                    MyTestInterface = new MyTestInterface()
                    {
                        MyInterfaceDoubleInterface = 2345,
                        //MyInterface2Int = 234345
                    }
                },
                MyEnumerable = new List<TestEnumerable>()
                {
                    new TestEnumerable()
                    {
                        MyProperty = "adsfasdfasfsadf"
                    }
                }
            };

            Stopwatch watch;

            var self = new List<long>();
            var auto = new List<long>();

            for (int j = 0; j < 10; j++)
            {
                watch = new Stopwatch();
                watch.Start();
                for (int i = 0; i < 10000; i++)
                {
                    var dto = new TestDTO()
                    {
                        MyIntProp1 = val.MyIntProp1,
                        MySuperFancyIntProp2 = val.MyIntProp2,
                        MySubTypeProp = new SubTypeDTO()
                        {
                            MyTestInterface = new MyTestInterfaceDTO()
                            {
                                MyInterfaceDoubleInterface = val.MySubTypeProp.MyTestInterface.MyInterfaceDoubleInterface
                            }
                        },
                        MyEnumerable = new List<TestEnumerableDTO>()
                    };

                    dto.MyEnumerable.Concat(val.MyEnumerable.Select(t => new TestEnumerableDTO() { MyProperty = t.MyProperty }));
                }
                watch.Stop();
                self.Add(watch.ElapsedMilliseconds);
                
                watch = new Stopwatch();
                watch.Start();
                for (int i = 0; i < 10000; i++)
                {
                    var dto = mapper.Map<Test, TestDTO>(val);
                }
                watch.Stop();
                auto.Add(watch.ElapsedMilliseconds);
            }
            Console.WriteLine(self.Average());
            Console.WriteLine(auto.Average());

            Console.ReadLine();
        }
    }

    class Test
    {
        public int MyIntProp1 { get; set; }
        public int MyIntProp2 { get; set; }
        public SubType MySubTypeProp { get; set; }
        public CtorSubType MyCtorSubType { get; set; }
        public IEnumerable<TestEnumerable> MyEnumerable { get; set; }
    }

    public class CtorSubType
    {
        public int CtorOnlyProp { get; }

        public CtorSubType(int ctorOnlyProp)
        {
            CtorOnlyProp = ctorOnlyProp;
        }
    }

    public class SubType
    {
        public ITestInterface MyTestInterface { get; set; }
    }

    public interface ITestInterface
    {
        double MyInterfaceDoubleInterface { get; set; }
    }

    public class MyTestInterface : ITestInterface
    {
        public double MyInterfaceDoubleInterface { get; set; }
    }

    public class MyTestInterface2 : ITestInterface
    {
        public double MyInterfaceDoubleInterface { get; set; }
        public int MyInterface2Int { get; set; }
    }

    class TestEnumerable
    {
        public string MyProperty { get; set; }
    }

    class TestDTO
    {
        public int MyIntProp1 { get; set; }
        public int MySuperFancyIntProp2 { get; set; }
        public SubTypeDTO MySubTypeProp { get; set; }
        public CtorSubTypeDTO MyCtorSubType { get; set; }
        public IEnumerable<TestEnumerableDTO> MyEnumerable { get; set; }
    }

    public class CtorSubTypeDTO
    {
        public int CtorOnlyProp { get; }

        public CtorSubTypeDTO(int ctorOnlyProp)
        {
            CtorOnlyProp = ctorOnlyProp;
        }
    }

    public class SubTypeDTO
    {
        public ITestInterfaceDTO MyTestInterface { get; set; }
    }

    public interface ITestInterfaceDTO
    {
        double MyInterfaceDoubleInterface { get; set; }
    }

    public class MyTestInterfaceDTO : ITestInterfaceDTO
    {
        public double MyInterfaceDoubleInterface { get; set; }
    }

    public class MyTestInterfaceDTO2 : ITestInterfaceDTO
    {
        public double MyInterfaceDoubleInterface { get; set; }
        public int MyInterface2Int { get; set; }
    }

    class TestEnumerableDTO
    {
        public string MyProperty { get; set; }
    }
}
